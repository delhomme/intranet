# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'Prise'
        db.create_table('prises', (
            ('cablage_effectue', self.gf('django.db.models.fields.BooleanField')(default=False, blank=True)),
            ('commentaire', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True, blank=True)),
            ('crans', self.gf('django.db.models.fields.BooleanField')(default=False, blank=True)),
            ('prise_crous', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('batiment', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('chambre', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('crous', self.gf('django.db.models.fields.BooleanField')(default=False, blank=True)),
            ('prise_crans', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('prises', ['Prise'])
    
    
    def backwards(self, orm):
        
        # Deleting model 'Prise'
        db.delete_table('prises')
    
    
    models = {
        'prises.prise': {
            'Meta': {'object_name': 'Prise', 'db_table': "'prises'"},
            'batiment': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'cablage_effectue': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'chambre': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'commentaire': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'crans': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'crous': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prise_crans': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'prise_crous': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }
    
    complete_apps = ['prises']
