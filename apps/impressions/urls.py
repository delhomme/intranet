from django.conf.urls import patterns, url 

import views

urlpatterns = patterns('',
    url('^$', views.view, name="view"),
    url('^gestion/$', views.gestion, name="gestion"),
    url('^gestion/delete/(?P<jid>[^/]*)/$', views.delete_job, name="delete"),
)
