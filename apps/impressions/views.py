# -*- encoding: utf-8 -*-

import settings

from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseBadRequest
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response

# Pour stocker des messages dans la session courante
from django.contrib import messages

# Récupération de la lib custom pour parler à l'imprimante
import get_jobs
import printLib

# Formulaires
from models import UploadForm, PrintForm

# config.impression, pour les prix
import sys
sys.path.append("/usr/scripts/gestion/")
import config.impression

config_prix = {"c_face_nb": config.impression.c_face_nb,
            "c_face_couleur": config.impression.c_face_couleur,
            "c_agrafe": config.impression.c_agrafe,
            "amm": config.impression.amm,
            "c_a4": config.impression.c_a4,
            "c_a3": config.impression.c_a3}

# Pour créer les dossier et faire des pdfinfo
import subprocess
import re
import os, os.path

def get_login(request):
    """Renvoie le login de l'utilisateur connecté"""
    # Attention, un intranet dirty hack fait que les users LDAP sont en login@crans.org
    login = request.user.username
    login = login.split("@")[0]
    return login

def is_imprimeur(request):
    """Renvoie True si l'utilisateur connecté à les droits imprimeurs"""
    li = request.user.groups.filter(name="crans_imprimeur")
    return len(li)!=0

class NotPdfFile(Exception):
    """Exception levée si le fichier uploadé n'est pas un pdf"""
    pass

def pdfinfo(filepath):
    """Renvoi le résultat d'un pdfinfo sur un fichier"""
    filename = os.path.basename(filepath)
    # On commence par vérifier qu'on a bien affaire à un pdf
    fileinfo = subprocess.Popen(["file", filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    fileinfo = fileinfo.communicate()[0]
    fileinfo = re.findall('%s: *(.*)' % (filename), fileinfo)
    if not "pdf document" in ''.join(fileinfo).lower():
        raise NotPdfFile
    infos = subprocess.Popen(["pdfinfo", filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    infos = infos.communicate()[0]
    infos = dict(re.findall('(.*?): *(.*)', infos))
    return infos

@login_required
def view(request):
    if request.method == "GET":
        uploadform = UploadForm(label_suffix=" :")
        return render_to_response("impressions/impressions.html",\
            {'uploadform': uploadform},\
            context_instance=RequestContext(request))
    else:
        if "SubmitUploadForm" in request.POST.keys():
            uploadform = UploadForm(request.POST, request.FILES, label_suffix=" :")
            if uploadform.is_valid():
                fichier = request.FILES["fichier"]
                # On crée le dossier d'impression (si il existe déjà, ça ne fait rien de particulier)
                login = get_login(request)
                createdir = subprocess.Popen(["sudo", "/usr/scripts/utils/chown_impressions.sh", login], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                createdir.communicate()
                # On y enregistre ensuite le fichier
                filename = fichier.name
                filepath = "/home/impressions/%s/%s" % (login, filename)
                with open(filepath, 'wb+') as destination:
                    for chunk in fichier.chunks():
                        destination.write(chunk)
                # On cherche les infos sur le fichier
                try:
                    infos = pdfinfo(filepath)
                except NotPdfFile:
                    messages.error(request, u"""Votre fichier %s n'est pas un fichier PDF.""" % (filename))
                    return render_to_response("impressions/impressions.html",\
                        {'uploadform': uploadform},\
                        context_instance=RequestContext(request))
                try:
                    pageno = int(infos["Pages"])
                except (KeyError, ValueError) as e:
                    messages.error(request, u"""Impossible de récupérer les informations sur le fichier PDF.""" )
                    return render_to_response("impressions/impressions.html",\
                        {'uploadform': uploadform},\
                        context_instance=RequestContext(request))
                printform = PrintForm(label_suffix=" :")
                return render_to_response("impressions/print.html",
                    {'printform': printform,
                     'pageno': pageno,
                     'une_agrafe': ["hg", "hd", "bg", "bd"],
                     'deux_agrafes': ["g", "d"],
                     'filename': filename,
                     'config_prix': config_prix},
                    context_instance=RequestContext(request))
            else:
                return render_to_response("impressions/impressions.html",\
                    {'uploadform': uploadform},\
                    context_instance=RequestContext(request))
        elif "SubmitPrintForm" in request.POST.keys():
            return HttpResponse(u"""Not implemented yet, come back later.""", status=501)
        else:
            return HttpResponseBadRequest(u"Formulaire non pris en charge.")

@login_required
def gestion(request):
    login = get_login(request)
    lpq_jobs = printLib.getLpqJobs()
    current_jobs, ended_jobs = get_jobs.get_both()
    if not is_imprimeur(request):
        # Si on n'est pas imprimeur, on ne garde que les taches de l'utilisateur courant
        ended_jobs = [i for i in ended_jobs if i.login == login]
        lpq_jobs = [i for i in lpq_jobs if i.login == login]
    imprimante_jobs = []
    return render_to_response("impressions/liste.html",
                              {"ended_jobs" : ended_jobs,
                               "current_jobs" : current_jobs,
                               "lpq_jobs" : lpq_jobs,
                               "imprimante_jobs" : imprimante_jobs},
                              context_instance=RequestContext(request))

@login_required
def delete_job(request, jid=-1):
    lpq_jobs = printLib.getLpqJobs()
    # On récupère le job
    try:
        jid = int(jid)
    except:
        pass
    lpq_jobs = [job for job in lpq_jobs if job.jid == jid]
    if lpq_jobs == []:
        messages.error(request, u"Cette tâche n'est plus dans la file d'attente du serveur.")
    else:
        job = lpq_jobs[0]
        login = get_login(request)
        if job.login == login or is_imprimeur(request):
            job.recrediter(login)
            job.cancel()
            messages.success(request, u"La tâche %s a été annulée." % (job.file))
        else:
            messages.error(request, u"Vous n'êtes pas le propriétaire de cette tâche, vous ne pouvez pas l'annuler.")
    return HttpResponseRedirect("/impressions/gestion/")
