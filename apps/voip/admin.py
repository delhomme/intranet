# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Profile, Call

admin.site.register(Profile)
admin.site.register(Call)
