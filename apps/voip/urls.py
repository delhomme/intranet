# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url 

import views

urlpatterns = patterns('',
    url('^$', views.index, name="index"),
    url('^edit/$', views.edit, name="edit"),
    url('^delete/$', views.delete, name="delete"),
    url('^book/$', views.book, name="book"),
    url('^book/download$', views.book, {'download': True}, name="book_download",),
    url('^history/sent/$', views.history, {'received': False}, name='sent_calls'),
    url('^history/sent/(?P<page>[0-9]+)$', views.history, {'received': False}),
    url('^history/$', views.history, name="received_calls"),
    url('^history/(?P<page>[0-9]+)$', views.history),
)
