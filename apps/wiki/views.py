# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.template import RequestContext
from forms import LinkAccount, CreateAccount
import settings
if settings.LOCATION != 'perso':
    from models_ldap import WikiName
else:
    from models_test import WikiName

def force_reconnect_to_wiki(request, wiki_name=None):
    url = request.GET.get('wiki_url', 'https://wiki.crans.org')
    action = request.GET.get('action', None)
    wiki_cas_url = "%slogout?service=%s%%3Faction%%3Dlogin_cas%%26force%%3Dforce" % (settings.CAS_SERVER_URL, url)

    if wiki_name and action == 'login_cas' and url:
        return HttpResponseRedirect(wiki_cas_url)
    else:
        return False

@login_required
@sensitive_post_parameters('username', 'password1', 'password2')
def create(request):
    url = request.GET.get('wiki_url', 'https://wiki.crans.org')
    action = request.GET.get('action', None)
    if request.method == 'POST':
        form = CreateAccount(request.POST)
        if form.is_valid():
            message, erreur = WikiName.create_account(request.user,
                                    form.cleaned_data['wiki_name'],
                                    form.cleaned_data['password1'])

            if erreur:
                messages.warning(request, message)
                if "L'adresse %s est d" % WikiName.get_canonical_email_from_user(request.user) in message:
                    messages.warning(request, 'Vous avez probablement déjà créer un compte Wiki, essayez plutôt de l\'associer ou de <a href="https://wiki.crans.org/?action=recoverpass">récupérer</a> votre mot de passe.')
                    messages.warning(request, 'En cas de problème, contactez nounou@crans.org')
                    return redirect('wiki:index')
                return redirect('wiki:create')

            WikiName.set_value_from_user(request.user,
                                    form.cleaned_data['wiki_name'])
            wikiname = WikiName.get_value_from_user(request.user)
            redirect_wiki = force_reconnect_to_wiki(request, wikiname)
            if redirect_wiki:
                return redirect_wiki
            else:
                messages.success(request, message)
                messages.warning(request,
                  u'Vous devrez vous <a href="%s">déconnecter</a> pour que la nouvelle association soit prise en compte.' % reverse('logout'))
                return redirect('wiki:index')
    else:
        form = CreateAccount()

    wikiname = WikiName.get_value_from_user(request.user)
    redirect_wiki = force_reconnect_to_wiki(request, wikiname)
    if redirect_wiki:
        return redirect_wiki
    if wikiname:
        return redirect('wiki:index')

    return render(request, "wiki/create.html", {
            'create_account': form,
            'wiki_name': wikiname,
            'action': action,
            'wiki_url': url,
            'canonical_wikiname': WikiName.get_canonical_from_user(request.user),
            'canonical_email': WikiName.get_canonical_email_from_user(request.user)
        })

@login_required
@sensitive_post_parameters('username', 'password')
def index(request):
    url = request.GET.get('wiki_url', 'https://wiki.crans.org')
    action = request.GET.get('action', None)
    if request.method == 'POST':
        form = LinkAccount(request.POST)
        if form.is_valid():
            WikiName.set_value_from_user(request.user,
                                    form.cleaned_data['wiki_name'])

            wikiname = WikiName.get_value_from_user(request.user)
            redirect_wiki = force_reconnect_to_wiki(request, wikiname)
            if redirect_wiki:
                return redirect_wiki
            else:
                messages.success(request,
                  u'Votre compte Wiki a bien été lié à votre compte Cr@ns.')
                messages.warning(request,
                  u'Vous devrez vous <a href="%s">déconnecter</a> pour que la nouvelle association soit prise en compte.' % reverse('logout'))
                return redirect('wiki:index')
    else:
        form = LinkAccount()

    wikiname = WikiName.get_value_from_user(request.user)
    redirect_wiki = force_reconnect_to_wiki(request, wikiname)
    if redirect_wiki:
        return redirect_wiki

    return render(request, "wiki/index.html", {
            'link_account': form,
            'wiki_name': wikiname,
            'action': action,
            'wiki_url': url,
        })

@login_required
def forget(request):
    """Set default wiki name"""
    if request.method == 'POST':
        WikiName.set_value_from_user(request.user, None)
        messages.success(request, u"Compte Wiki désassocié avec succès.")
        return redirect('wiki:index')
    return render(request, "confirm.html", {
        'confirm_title': u"Oublier l'association Wiki",
        'confirm_message': u'Êtes-vous sûr de vouloir désassocier vos comptes Wiki et Cr@ns ?',
        'confirm_cancel_url': reverse('wiki:index'),
    })
