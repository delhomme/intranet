# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from models import Code

urlpatterns = patterns('apps.digicode.views',
    url(r'^$', 'view_code'),
    url(r'^delete/$', 'delete_code'),
    url(r'^create/(?P<digits>[0-9]{%s})?$' % Code.DIGITS_MAXLENGTH, 'create_code'),
    url(r'^list/(?P<owner>[^/]*)$', 'list_code'),
)
