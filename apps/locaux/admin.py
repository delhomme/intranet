
import models
from django.contrib import admin

admin.site.register(models.Local)
admin.site.register(models.Clef)
admin.site.register(models.Intervention)
