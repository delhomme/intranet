from django import template
register = template.Library()

@register.filter('full_name')
def full_name(user):
    return user.first_name + ' ' + user.last_name
