# -*- coding: utf-8 -*-
#
# IMPRESSION -- Interface d'impression dans le nouvel intranet
#
# Copyright (C) 2010 Antoine Durand-Gasselin
# Authors: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import with_statement

import re, os, sys, tempfile, shutil

from commands import getoutput, mk2arg

import django.shortcuts
from django.template import RequestContext

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.urlresolvers import reverse

try:
    from settings import db
except Exception:
    pass

import logging, new_impression

from models import ImpressionFile
from forms import create_user_form
from django.contrib.auth.models import User

BOOK_REGEXP = re.compile('-(a.)?book.pdf')

def getPDFs(request):
    '''Récupère la liste des fichiers accessibles à l'utilisiteur'''
    file_folder = get_path(request)
    if not os.path.isdir(file_folder):
        return []
    liste = os.listdir(file_folder)
    list_pdf = []
    # exclusion des fichiers qui ne sont pas des PDF
    for f in liste:
        if getoutput('file -ib ' + mk2arg(file_folder, f)).startswith("application/pdf"):
            if not BOOK_REGEXP.search(f):
                list_pdf.append(f)
    return list_pdf

def savePDF(aFile, request):
    '''Enregistre le fichier passé en argument pour l'utilisateur loggé'''
    file_folder = get_path(request)
    if not os.path.isdir(file_folder):
        os.makedirs(file_folder, mode=0750)
    fname = re.sub("[^\w\._]", "", re.sub("\s+", "_", aFile._name))
    fpath = file_folder + fname
    with open(fpath, 'w+b') as f:
        shutil.copyfileobj(aFile.file, f)
    os.chmod(fpath, 0640)
    logging.log("New file uploaded at : %s" % fpath, "IMPRESSION")
    return fname

@login_required
def select(request):
    SelectForm = create_user_form(request.user)
    if request.method == 'POST':
        form = SelectForm(request.POST, request.FILES)
        if form.is_valid():
            select_file = form.cleaned_data['select_file']
            upload_file = form.cleaned_data['upload_file']
            if upload_file:
                newfile = ImpressionFile(owner = request.user, name = upload_file.name, impression_file = upload_file)
                newfile.save()
                return HttpResponseRedirect(reverse("params", args = [newfile]))
                    
            elif select_file:
                return HttpResponseRedirect(reverse("params", args = [select_file]))                                             
    else:
        form = SelectForm()
    return django.shortcuts.render_to_response("impression/select.html", locals(), context_instance=RequestContext(request))

@login_required
def params(request, filename=None):
    '''Affiche les paramètres d'impression d'un fichier'''
    if not filename: return HttpResponseRedirect("impression/select")

    f = ImpressionFile.objects.get(owner = request.user, name = filename)
    
    fpath = f.impression_file.path
    if not request.session.has_key('impression') or request.session['impression']._fichier != fpath:
        request.session['impression'] = new_impression.impression(fpath)

    mods = {}
    for param,value in request.POST.items():
        mods[str(param)] = value

    prix = request.session['impression'].changeSettings(**mods)
    nb_pages = request.session['impression']._nb_pages
    params = request.session['impression']._settings
    session = request.session
    solde = session.get('solde')
    avail_papier = new_impression.DICT_PAPIER
    avail_agrafage = new_impression.DICT_AGRAFAGE

    if request.POST.get('do_print') == u'Imprimer':
        return do_print(request)

    return django.shortcuts.render_to_response("impression/params.html", locals())

@user_passes_test(lambda u: u.groups.get(name='crans_nounou'))
def do_print(request):
    session = request.session
    adh = db.search(u'uid=%s' % request.user.username.split("@")[0], 'w')['adherent'][0]
    session['impression'].imprime(adh)
    del adh
    return django.shortcuts.render_to_response("dummy/bonjour.html", locals())

