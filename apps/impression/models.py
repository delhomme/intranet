#!/usr/bin/env python
# -!- encoding: utf-8 -!-

from django.db import models
from django.core.files.storage import FileSystemStorage

from django.contrib.auth.models import User

fs = FileSystemStorage(location='/var/impression/fichiers')

def get_file_path(instance, filename):
    """Récupère le chemin vers lequel uploader le fichier"""
    ownername, domainname = instance.owner.username.split("@")

    if domainname.startswith('club-'):
        ownername, _ = domainname.split(".", 1)

    return "%s/%s" % (ownername, filename)


class ImpressionFile(models.Model):
    """Fichier pour l'impression"""
    owner = models.ForeignKey(User)
    name = models.CharField(max_length = 100)
    impression_file = models.FileField(upload_to = get_file_path, storage = fs)

    def __unicode__(self):
        return u"%s" % self.name
