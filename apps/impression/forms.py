#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# FORMS.PY -- Formulaires Django pour l'impression
#
# Copyright (C) 2010 Nicolas Dandrimont
# Authors: Nicolas Dandrimont <olasd@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import Form, ChoiceField, FileField, Select

from models import ImpressionFile

def create_user_form(user):

    files = ImpressionFile.objects.filter(owner=user)
    
    class SelectForm(Form):
        select_file = ChoiceField(choices = [(f.name, f) for f in files], widget = Select(attrs={'size': '5'}), required = False)
        upload_file = FileField(required = False)
        
    return SelectForm
