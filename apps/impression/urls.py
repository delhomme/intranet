from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url('^$', views.select, name="root"),
    url('^select$', views.select, name="select"),
    url('^params/(?P<filename>.*)$', views.params, name="params"),
    url('^do_print$', views.do_print, name="do_print"),
)


